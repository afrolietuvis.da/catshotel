CREATE TABLE `client` (
                          `id` bigint NOT NULL AUTO_INCREMENT,
                          `name` varchar(45) NOT NULL,
                          `email` varchar(45) NOT NULL,
                          `phone` varchar(45) NOT NULL,
                          `password` varchar(45) NOT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `hotel` (
                         `id` bigint NOT NULL AUTO_INCREMENT,
                         `name` varchar(45) NOT NULL,
                         `address` varchar(45) NOT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `reservation` (
                               `id` bigint NOT NULL AUTO_INCREMENT,
                               `date_from` date NOT NULL,
                               `date_to` date NOT NULL,
                               `is_paid` bit(1) NOT NULL,
                               `room_id` bigint NOT NULL,
                               `client_id` bigint NOT NULL,
                               PRIMARY KEY (`id`),
                               KEY `fk_reservation_room1_idx` (`room_id`),
                               KEY `fk_reservation_client1_idx` (`client_id`),
                               CONSTRAINT `fk_reservation_client1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
                               CONSTRAINT `fk_reservation_room1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `room` (
                        `id` bigint NOT NULL AUTO_INCREMENT,
                        `floor` int NOT NULL,
                        `number` int NOT NULL,
                        `room_type` varchar(45) NOT NULL,
                        `hotel_id` bigint NOT NULL,
                        PRIMARY KEY (`id`),
                        KEY `fk_room_hotel1_idx` (`hotel_id`),
                        CONSTRAINT `fk_room_hotel1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
SELECT * FROM catshotel.reservation;