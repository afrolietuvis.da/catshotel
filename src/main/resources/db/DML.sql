INSERT INTO hotel (id,name,address) values(1,'Hotel Cats','Cats Street 14');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(1,1,1,150,'Single');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(2,1,1,140,'Double');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(3,1,1,130,'Single');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(4,1,1,120,'VIP');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(5,1,1,110,'Double');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(6,1,2,230,'VIP');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(7,1,2,220,'Double');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(8,1,2,210,'Single');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(9,1,3,320,'Double');
INSERT INTO room (id,hotel_id,floor,number,room_type)
values(10,1,3,310,'VIP');
