package UI;

import entities.ClientEntity;
import entities.ReservationEntity;
import entities.RoomEntity;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import repositories.ClientRepository;
import repositories.ReservationRepository;
import repositories.RoomRepository;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class ReservationStage extends Stage {

    private ClientEntity client;
    private RoomRepository roomRepository;
    private ReservationRepository reservationRepository = new ReservationRepository();
//    ImageStage imageStage;


    ReservationStage(ClientEntity client) {

        ClientRepository clientRepository = new ClientRepository();
        this.client = clientRepository.findOne(client.getId());

        Map<Integer, RoomEntity> rooms = initializeRooms();
        Map<String, List<RoomEntity>> roomTypeList = sortRoomsByType();

        //ROOT
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(0, 0, 5, 0));

        //MENU
        Menu menu;
        MenuBar menuBar = new MenuBar();
        menu = new Menu("Navigate");
        Menu subProfileMenu = new Menu("Profile");
        MenuItem reservationMenu = new MenuItem("New Reservation");
        MenuItem logoutMenu = new MenuItem("Logout");
        MenuItem viewProfile = new MenuItem("My Reservations");
        MenuItem editProfile = new MenuItem("My Profile");
        subProfileMenu.getItems().addAll(viewProfile, editProfile);
        menu.getItems().addAll(subProfileMenu, reservationMenu, logoutMenu);
        menuBar.getMenus().add(menu);
        root.setTop(menuBar);

        logoutMenu.setOnAction(e -> {
            new LoginStage();
            this.close();
        });
        reservationMenu.setOnAction(e -> {
        });
        viewProfile.setOnAction(e -> {
            new ProfileStage(this.client);
            this.close();
        });
        editProfile.setOnAction(e -> {
            new EditProfileStage(this.client);
            this.close();
        });

        //MAIN VBOX
        VBox mainBox = new VBox();
        root.setCenter(mainBox);
        mainBox.setAlignment(Pos.CENTER);


        HBox welcomeBox = new HBox();
        GridPane pane = new GridPane();


        //COMBOBOX
        ComboBox<String> chooseTypeComboBox = new ComboBox<>();
        ObservableList<String> data = FXCollections.observableArrayList(roomTypeList.keySet());
        chooseTypeComboBox.itemsProperty().setValue(data);

        //LABELS
        Label selectRoomLabel = new Label("Select Room Type");
        selectRoomLabel.setFont(Font.font(13));
        Label titleLabel = new Label("Reservation From");
        titleLabel.setFont(Font.font(25));

        //WELCOME BOX WITH TITLE
        welcomeBox.getChildren().add(titleLabel);
        welcomeBox.setAlignment(Pos.CENTER);

        //BUTOONS AND DATEPICKERS
        Button makeReservation = new Button("Make Reservation");
        DatePicker startDate = new DatePicker();
        DatePicker endDate = new DatePicker();
        startDate.setPromptText("Pick Check in date");
        endDate.setPromptText("Pick Check out date");

        GridPane.setHalignment(titleLabel, HPos.CENTER);
        pane.add(selectRoomLabel, 1, 1);
        pane.add(chooseTypeComboBox, 1, 2);
        GridPane.setFillWidth(chooseTypeComboBox, true);
        pane.add(startDate, 2, 1);
        pane.add(endDate, 2, 2);
        pane.setVgap(5);
        pane.setHgap(9);


        mainBox.getChildren().addAll(welcomeBox, pane);
        mainBox.setSpacing(10);
        mainBox.setPadding(new Insets(10, 10, 10, 10));


        ListView<RoomEntity> listView = new ListView<>();
        ObservableList<RoomEntity> roomData = FXCollections.observableArrayList(rooms.values());
        listView.setItems(roomData);
        mainBox.getChildren().add(listView);
        mainBox.getChildren().add(makeReservation);
        mainBox.setAlignment(Pos.CENTER);

        chooseTypeComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    listView.setItems(getRoomList(new ArrayList<>(roomTypeList.get("All rooms")), newValue));
                }
            }
        });

//        listView.setOnMousePressed(e->{
//        imageStage = new ImageStage(listView.getSelectionModel().getSelectedItem().getRoomType(),
//                MouseInfo.getPointerInfo().getLocation().getX(),
//                MouseInfo.getPointerInfo().getLocation().getY()
//                );
//        });
//        listView.setOnMouseReleased(e->{
//            if (imageStage !=null)
//            imageStage.close();
//        });

        endDate.disableProperty().bind(listView.getSelectionModel().selectedItemProperty().isNull());
        startDate.disableProperty().bind(listView.getSelectionModel().selectedItemProperty().isNull());


            final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
                @Override
                public DateCell call(final DatePicker datePicker) {
                    return new DateCell() {
                        @Override
                        public void updateItem(LocalDate item, boolean empty) {
                            super.updateItem(item, empty);

                            RoomEntity room = roomRepository.findOne(listView.getSelectionModel().getSelectedItem().getId());
                            for (ReservationEntity reservation : room.getReservations()) {
                                if ((item.isAfter(reservation.getStartDate()) || item.equals(reservation.getStartDate()))
                                        && (item.isBefore(reservation.getEndDate()) || item.equals(reservation.getEndDate()))){
                                    setStyle("-fx-background-color: #ff746d;");
                                    setDisable(true);
                                }
                            }
                        }
                    };

                }
            };
        endDate.setDayCellFactory(dayCellFactory);
        startDate.setDayCellFactory(dayCellFactory);



        makeReservation.setOnAction(e -> {

                    if (listView.getSelectionModel().getSelectedItem() == null) {
                        JOptionPane.showMessageDialog(null,
                                "Please select a room!", "Warning", JOptionPane.WARNING_MESSAGE);
                        return;
                    }

                    if (startDate.getValue() == null || endDate.getValue() == null) {
                        JOptionPane.showMessageDialog(null,
                                "Please select a checkin and checkout dates!", "Warning", JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                    if (startDate.getValue().equals(endDate.getValue())) {
                        JOptionPane.showMessageDialog(null,
                                "Checkin and check out dates cant match!", "Warning", JOptionPane.WARNING_MESSAGE);
                        return;
                    } else if (startDate.getValue().isAfter(endDate.getValue())) {
                        JOptionPane.showMessageDialog(null,
                                "You can't checkin tomorrow and checkout yesterday\ntraveling in time is illegal!", "Warning", JOptionPane.WARNING_MESSAGE);
                        return;
                    } else if (LocalDate.now().isAfter(endDate.getValue())) {
                        JOptionPane.showMessageDialog(null,
                                "Sorry, we can't make a reservation in the past!", "Warning", JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                    LocalDate startDateValue = startDate.getValue();
                    LocalDate endDateValue = endDate.getValue();
                    ReservationEntity newReservation = new ReservationEntity();
                    newReservation.setClient(client);
                    newReservation.setPaid(false);
                    newReservation.setRoom(listView.getSelectionModel().getSelectedItem());
                    newReservation.setStartDate(startDateValue);
                    newReservation.setEndDate(endDateValue);
                    reservationRepository.save(newReservation);
                    this.client = clientRepository.findOne(this.client.getId());
                    JOptionPane.showMessageDialog(null, "Reservation successful", "Done", JOptionPane.INFORMATION_MESSAGE);
                    this.close();
                    new ReservationStage(this.client);
                }
        );

        Scene reservationScene = new Scene(root, 500, 500);
        this.setScene(reservationScene);
        this.setResizable(false);
        this.show();
    }


//    private List<LocalDate> getAllDatesFromRoomReservation(RoomEntity room) {
//        List<LocalDate> dates = new ArrayList<>();
//        for (ReservationEntity reservation : room.getReservations()) {
//            dates.addAll(getAllDaysFromRange(reservation));
//        }
//        return dates;
//    }

    private ObservableList<RoomEntity> getRoomList(ArrayList<RoomEntity> list, String type) {
        ArrayList<RoomEntity> rooms = new ArrayList<>();
        for (RoomEntity room : list) {
            if (room.getRoomType().equals(type))
                rooms.add(room);
        }
        if (rooms.isEmpty())
            return FXCollections.observableArrayList(list);
        else
            return FXCollections.observableArrayList(rooms);
    }

    private Map<Integer, RoomEntity> initializeRooms() {

        roomRepository = new RoomRepository();

        List<RoomEntity> roomEntities = roomRepository.findAll();
        Map<Integer, RoomEntity> rooms = new HashMap<>();
        for (RoomEntity roomEntity : roomEntities) {
            rooms.put(roomEntity.getRoomNumber(), roomEntity);
        }
        return rooms;
    }

    private Map<String, List<RoomEntity>> sortRoomsByType() {
        Map<String, List<RoomEntity>> roomEntityMap = new HashMap<>();
        List<RoomEntity> rooms = roomRepository.findAll();
        for (RoomEntity roomEntity : rooms) {
            roomEntityMap.putIfAbsent(roomEntity.getRoomType(), new ArrayList<RoomEntity>());
            roomEntityMap.get((roomEntity.getRoomType())).add(roomEntity);
        }
        roomEntityMap.put("All rooms", new ArrayList<RoomEntity>(roomRepository.findAll()));
        return roomEntityMap;
    }

    private List<LocalDate> getAllDaysFromRange(ReservationEntity reservation) {
        long numOfDaysBetween = ChronoUnit.DAYS.between(reservation.getStartDate(), reservation.getEndDate());
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(reservation.getStartDate()::plusDays)
                .collect(Collectors.toList());
    }
}
