package UI;

import entities.ClientEntity;
import entities.ReservationEntity;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import repositories.ClientRepository;

class ProfileStage extends Stage {

    private ClientEntity client;
    private static ReservationStage reservationStage;

    ProfileStage(ClientEntity client) {

        ClientRepository clientRepository = new ClientRepository();
        this.client = clientRepository.findOne(client.getId());
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(0, 5, 0, 5));
        VBox mainBox = new VBox();
        Button newReservationButton = new Button("Make new reservation");
        Label titleLabel = new Label("Your Reservations:");
        titleLabel.setFont(Font.font(25));
        mainBox.getChildren().add(titleLabel);
        root.setCenter(mainBox);
        ObservableList<ReservationEntity> reservationEntityObservableList = FXCollections.observableArrayList(this.client.getReservations());
        ListView<ReservationEntity> reservationEntityListView = new ListView<>();
        reservationEntityListView.setItems(reservationEntityObservableList);

        mainBox.getChildren().add(reservationEntityListView);
        HBox loggedInAsBox = new HBox();
        Label loggedInAsLabel = new Label("Logged in as: ");
        Label clientNameLabel = new Label(client.getName());
        loggedInAsLabel.setFont(Font.font(15));
        clientNameLabel.setFont(Font.font(15));
        Hyperlink logOutLink = new Hyperlink("Log out");
        loggedInAsBox.getChildren().addAll(loggedInAsLabel, clientNameLabel, logOutLink);
        mainBox.getChildren().add(loggedInAsBox);
        loggedInAsBox.setAlignment(Pos.BOTTOM_RIGHT);
        mainBox.setAlignment(Pos.CENTER);
        logOutLink.setFont(Font.font(12));


        logOutLink.setOnAction(e -> {
            new LoginStage();
            this.close();
        });


        //MENU
        Menu menu;
        MenuBar menuBar = new MenuBar();
        menu = new Menu("Navigate");
        Menu subProfileMenu = new Menu("Profile");
        MenuItem logoutMenu = new MenuItem("Logout");
        MenuItem viewProfile = new MenuItem("View Profile");
        MenuItem editProfile = new MenuItem("Edit Profile");
        subProfileMenu.getItems().addAll(viewProfile, editProfile);
        menu.getItems().addAll(subProfileMenu, logoutMenu);
        menuBar.getMenus().add(menu);
        root.setTop(menuBar);

        logoutMenu.setOnAction(e -> {
            new LoginStage();
            this.close();
            reservationStage.close();
        });

        newReservationButton.setOnAction(e -> {
            if (reservationStage == null)
           reservationStage = new ReservationStage(this.client);
            else {
                reservationStage.isShowing();
            }
        });

        viewProfile.setOnAction(e -> {
        });
        editProfile.setOnAction(e -> {
            new EditProfileStage(this.client);
            this.close();
        });

        this.setOnShowing(event -> refreshReservations());


        Scene scene = new Scene(root, 750, 500);
        this.setResizable(false);
        this.setScene(scene);
        this.show();
    }

    private void refreshReservations() {
        ObservableList<ReservationEntity> reservationEntityObservableList = FXCollections.observableArrayList(client.getReservations());
        ListView<ReservationEntity> reservationEntityListView = new ListView<>();
        reservationEntityListView.setItems(reservationEntityObservableList);
    }


}
