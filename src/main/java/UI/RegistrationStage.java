package UI;

import entities.ClientEntity;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import repositories.ClientRepository;

import javax.swing.*;
import java.util.List;

class RegistrationStage extends Stage {

    private Label verifyName = new Label();
    private Label verifyPassword = new Label();
    private Label verifyPassword2 = new Label();
    private Label verifyEmail = new Label();
    private Label verifyPhone = new Label();
    private TextField nameField = new TextField();
    private TextField emailField = new TextField();
    private TextField phoneField = new TextField();
    private PasswordField passwordField = new PasswordField();
    private PasswordField passwordField2 = new PasswordField();
    private Button submitButton = new Button("Submit");

    RegistrationStage(){

        Tooltip pwTip = new Tooltip("Password should be at least 7 characters length");
        Tooltip nameTip = new Tooltip("Enter your name");
        Tooltip emailTip = new Tooltip("Enter your email");
        Tooltip phoneTip = new Tooltip("Enter your phone number");

        nameField.setPromptText("Enter your name");
        nameField.setTooltip(nameTip);
        passwordField.setPromptText("Enter your password");
        passwordField.setTooltip(pwTip);
        passwordField2.setPromptText("Reenter password");
        passwordField2.setTooltip(pwTip);
        emailField.setPromptText("Enter your email");
        emailField.setTooltip(emailTip);
        phoneField.setPromptText("Enter you phone number");
        phoneField.setTooltip(phoneTip);

        VBox vBox = new VBox();
        Label label = new Label("Registration Form");
        Label warningLabel = new Label();
        vBox.getChildren().addAll(label,nameField,verifyName,passwordField,verifyPassword,
                passwordField2,verifyPassword2,emailField,verifyEmail,phoneField,verifyPhone, submitButton, warningLabel);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(5);

        GridPane root = new GridPane();
        root.setVgap(5);
        root.setHgap(5);
        root.setAlignment(Pos.CENTER);
        root.getChildren().add(vBox);
        warningLabel.setTextFill(Color.RED);

        emailField.setOnKeyReleased(e ->{
            validateSubmit();
            boolean taken = emailField.getText().equals("taken");
            if (taken){
                verifyEmail.setTextFill(Color.RED);
                verifyEmail.setText("Email already registered");
            } else {
                verifyEmail.setTextFill(Color.GREEN);
                verifyEmail.setText("OK");
            }
        });

        phoneField.setOnKeyReleased(e -> {
            validateSubmit();
            boolean taken = phoneField.getText().equals("taken");
            if(taken){
                verifyPhone.setTextFill(Color.RED);
                verifyPhone.setText("Phone number already registered");
            } else {
                verifyPhone.setTextFill(Color.GREEN);
                verifyPhone.setText("OK");
            }
        }); //
        passwordField.setOnKeyReleased(e -> {
            validateSubmit();
            if (passwordField.getText().length()<7){
                verifyPassword.setTextFill(Color.RED);
                verifyPassword.setText("Password is too short");
            } else {
                verifyPassword.setTextFill(Color.GREEN);
                verifyPassword.setText("OK");
            }
        });
        passwordField2.setOnKeyReleased(e -> {
            validateSubmit();
            boolean match = passwordField.getText().equals(passwordField2.getText());
            if (match){
                verifyPassword2.setTextFill(Color.GREEN);
                verifyPassword2.setText("OK");
            } else {
                verifyPassword2.setTextFill(Color.RED);
                verifyPassword2.setText("Passwords don't match");
            }
        });

        nameField.setOnKeyReleased(e -> {
            validateSubmit();
            verifyName.setTextFill(Color.GREEN);
            verifyName.setText("OK");
        });



        submitButton.setOnAction(e -> {
            ClientRepository clientRepository = new ClientRepository();
            ClientEntity newClient = new ClientEntity();
            newClient.setName(nameField.getText());
            newClient.setEmail(emailField.getText());
            newClient.setPhone(phoneField.getText());
            newClient.setPassword(passwordField.getText());
            clientRepository.save(newClient);
            Long newClientId = getClientIdByPhoneNumber(clientRepository,newClient.getPhone());
            submitButton.setTextFill(Color.GREEN);
            submitButton.setText("Registration Successful");
            nameField.disableProperty().bind(submitButton.textProperty().isEqualTo("Registration Successful"));
            passwordField.disableProperty().bind(submitButton.textProperty().isEqualTo("Registration Successful"));
            passwordField2.disableProperty().bind(submitButton.textProperty().isEqualTo("Registration Successful"));
            emailField.disableProperty().bind(submitButton.textProperty().isEqualTo("Registration Successful"));
            phoneField.disableProperty().bind(submitButton.textProperty().isEqualTo("Registration Successful"));
            submitButton.disableProperty().bind(submitButton.textProperty().isEqualTo("Registration Successful"));
            JOptionPane.showMessageDialog( null,"Registration successful\nYour ID is " + newClientId + "\nUse it to login with your password","Success", JOptionPane.INFORMATION_MESSAGE);
        });

        submitButton.disableProperty().bind(validateSubmit());

        this.setScene(new Scene(root,200,350));
        this.setResizable(false);
        root.requestFocus();
        this.initStyle(StageStyle.UTILITY);
        this.show();
    }
    private ObservableValue<? extends Boolean> validateSubmit(){
        return (verifyPhone.textProperty().isNotEqualTo("OK")
                .or(verifyEmail.textProperty().isNotEqualTo("OK"))
                .or(verifyName.textProperty().isNotEqualTo("OK"))
                .or(verifyPassword.textProperty().isNotEqualTo("OK"))
                .or(verifyPassword2.textProperty().isNotEqualTo("OK")));
}

    private Long getClientIdByPhoneNumber(ClientRepository clientRepository, String phone){
        List<ClientEntity> clients = clientRepository.findAll();
        for(ClientEntity clientEntity:clients){
            if (clientEntity.getPhone().equals(phone)){
                return clientEntity.getId();
            }
        }
        return null;
    }

}
