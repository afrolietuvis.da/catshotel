package UI;

import entities.ClientEntity;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import repositories.ClientRepository;

class EditProfileStage extends Stage {

    private ClientEntity client;

    EditProfileStage(ClientEntity client){
        ClientRepository clientRepository = new ClientRepository();
        this.client = clientRepository.findOne(client.getId());

        //ROOT
        BorderPane root = new BorderPane();
        VBox titleBox = new VBox();
        Label titleLabel = new Label("Edit Profile");
        titleLabel.setFont(Font.font(25));
        titleBox.getChildren().add(titleLabel);
        root.setCenter(titleBox);
        titleBox.setAlignment(Pos.CENTER);

        //MENU
        Menu menu;
        MenuBar menuBar = new MenuBar();
        menu = new Menu("Navigate");
        Menu subProfileMenu = new Menu("Profile");
        MenuItem logoutMenu = new MenuItem("Logout");
        MenuItem viewProfile = new MenuItem("View Profile");
        MenuItem editProfile = new MenuItem("Edit Profile");
        subProfileMenu.getItems().addAll(viewProfile,editProfile);
        menu.getItems().addAll(subProfileMenu,logoutMenu);
        menuBar.getMenus().add(menu);
        root.setTop(menuBar);

        logoutMenu.setOnAction(e->{
            new LoginStage();
            this.close();
        });
        viewProfile.setOnAction(e->{
            new ProfileStage(this.client);
            this.close();
        });
        editProfile.setOnAction(e->{
        });

        Scene scene = new Scene(root,500,500);
        this.setScene(scene);
        this.show();
    }

}
