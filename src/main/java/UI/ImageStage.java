package UI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


class ImageStage extends Stage {

    ImageStage(String pic, double x,double y){
        System.out.println(pic);
        String imageString = "/images/"+pic+".jpg";
        System.out.println(imageString);
        Label imageLabel = new Label(pic+ " Room");
        imageLabel.setFont(Font.font(15));
        VBox myPane = new VBox();
        myPane.getChildren().add(imageLabel);
        myPane.setAlignment(Pos.CENTER);
        myPane.setPadding(new Insets(5,5,5,5));
        Image image = new Image(getClass().getResource(imageString).toString());
        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(myPane.widthProperty());
        imageView.fitHeightProperty().bind(myPane.heightProperty());
        myPane.getChildren().add(imageView);
        Scene myScene = new Scene(myPane,150,150);
        this.initStyle(StageStyle.UNDECORATED);
        this.setScene(myScene);
        this.setTitle("Room Preview");
        this.setX(x);
        this.setY(y);
        this.show();
    }
}
