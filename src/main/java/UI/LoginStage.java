package UI;

import entities.ClientEntity;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import repositories.ClientRepository;

import java.util.regex.Pattern;


class LoginStage extends Stage {

    private static RegistrationStage regStage;

    LoginStage(){

        //  Parent root = FXMLLoader.load(getClass().getResource("/fxml/Sample.fxml"));

        Label welcomeLabel = new Label("Welcome to our hotel");
        welcomeLabel.setFont(Font.font(15));
        TextField usernameField = new TextField();
        PasswordField passwordField = new PasswordField();

        Label username = new Label("Enter your ID:");
        Label password = new Label("Enter your password:");
        Button logButton = new Button("Login");
        Button regButton = new Button("Don't have an account?");
        Label validateLabel = new Label();
        regButton.setTextFill(Color.BLUE);

        passwordField.disableProperty().bind(usernameField.textProperty().isEmpty());
        logButton.disableProperty().bind(usernameField.textProperty().isEmpty().or(passwordField.textProperty().isEmpty()));

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(5);
        gridPane.setVgap(5);

        gridPane.add(username, 1, 0);
        gridPane.add(password, 1, 1);
        gridPane.add(usernameField, 2, 0);
        gridPane.add(passwordField, 2, 1);
        gridPane.add(logButton, 2, 2);
        gridPane.add(validateLabel, 2, 3);
        gridPane.add(regButton, 2, 4);
        regButton.setOnAction(e -> {
            if (regStage == null){
                regStage = new RegistrationStage();
            } else {
                regStage.isShowing();
            }
        });

        gridPane.setHalignment(welcomeLabel, HPos.CENTER);
        gridPane.setHalignment(logButton, HPos.RIGHT);
        gridPane.setHalignment(regButton, HPos.RIGHT);
        gridPane.setHalignment(validateLabel, HPos.RIGHT);

        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(welcomeLabel, gridPane);


        logButton.setOnAction(e -> {
            ClientRepository clientRepo = new ClientRepository();
            if (Pattern.matches("[a-zA-Z]+", usernameField.getText())) {
                validateLabel.setTextFill(Color.RED);
                validateLabel.setText("Invalid ID");
                return;
            }
            Long id = Long.parseLong(usernameField.getText());
            String pw = passwordField.getText();
            ClientEntity client = clientRepo.findOne(id);
            if (client == null) {
                validateLabel.setTextFill(Color.RED);
                validateLabel.setText("No user with this ID");
            } else if (client.getPassword().equals(pw)) {
                new MainStage(client);
                this.close();
            } else {
                validateLabel.setTextFill(Color.RED);
                validateLabel.setText("Incorrect password");
            }

        });

        Scene scene = new Scene(vBox, 280, 190);
        this.setScene(scene);
        this.setResizable(false);
        this.initStyle(StageStyle.UTILITY);
        this.show();
    }

}