package UI;

import entities.ClientEntity;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import repositories.ClientRepository;

public class MainStage  extends Stage {

    ClientEntity client;
    ReservationStage reservationStage;

    public MainStage(ClientEntity client){

        BorderPane root = new BorderPane();
        Menu menu;
        MenuBar menuBar = new MenuBar();
        menu = new Menu("Navigate");
        Menu subProfileMenu = new Menu("Profile");
        MenuItem logoutMenu = new MenuItem("Logout");
        MenuItem viewProfile = new MenuItem("View Profile");
        MenuItem editProfile = new MenuItem("Edit Profile");
        subProfileMenu.getItems().addAll(viewProfile, editProfile);
        menu.getItems().addAll(subProfileMenu, logoutMenu);
        menuBar.getMenus().add(menu);
        Button newReservationButton = new Button("Make new reservation");
        root.setTop(menuBar);

        logoutMenu.setOnAction(e -> {
            new LoginStage();
            this.close();
            if (reservationStage !=null && reservationStage.isShowing())
            reservationStage.close();
        });

        newReservationButton.setOnAction(e -> {
            if (reservationStage == null)
                reservationStage = new ReservationStage(this.client);
            else {
                reservationStage.isShowing();
            }
        });



        Scene scene = new Scene(root,300,300);
        this.setScene(scene);
        this.show();

    }

}
