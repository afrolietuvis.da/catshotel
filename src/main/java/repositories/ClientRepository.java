package repositories;

import entities.ClientEntity;
import services.ConnectionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.function.Consumer;

public class ClientRepository {
    private final EntityManager em;

    public ClientRepository() {
        em = ConnectionService.getSessionFactory().createEntityManager();
    }

    public List<ClientEntity> findAll() {
        String hqlQuery = "from ClientEntity p";
        return em.createQuery(hqlQuery, ClientEntity.class).getResultList();
    }

    public ClientEntity findOne(Long id) {
        return em.find(ClientEntity.class, id);
    }

    public ClientEntity save(ClientEntity client) {
        return persist(client, em::persist);
    }

    public ClientEntity update(ClientEntity client) {
        return persist(client, em::merge);
    }

    public void delete(ClientEntity client) {
        persist(client, em::remove);
    }

    private ClientEntity persist(ClientEntity client, Consumer<ClientEntity> consumer) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            consumer.accept(client);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }

        return client;
    }
}
