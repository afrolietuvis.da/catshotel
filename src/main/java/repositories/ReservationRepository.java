package repositories;

import entities.ClientEntity;
import entities.ReservationEntity;
import services.ConnectionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.function.Consumer;

public class ReservationRepository {

    private final EntityManager em;


    public ReservationRepository() {
        em = ConnectionService.getSessionFactory().createEntityManager();
    }

    public List<ReservationEntity> findAll() {
        String hqlQuery = "from ReservationEntity p";
        return em.createQuery(hqlQuery, ReservationEntity.class).getResultList();
    }

    public ReservationEntity findOne(Long id) {
        return em.find(ReservationEntity.class, id);
    }

    public ReservationEntity save(ReservationEntity reservation) {
        return persist(reservation, em::persist);
    }

    public ReservationEntity update(ReservationEntity reservation) {
        return persist(reservation, em::merge);
    }

    public void delete(ReservationEntity reservation) {
        persist(reservation, em::remove);
    }

    private ReservationEntity persist(ReservationEntity reservation, Consumer<ReservationEntity> consumer) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            consumer.accept(reservation);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }

        return reservation;
    }
}