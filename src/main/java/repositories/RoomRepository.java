package repositories;


import entities.RoomEntity;
import services.ConnectionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.function.Consumer;


public class RoomRepository {
    private final EntityManager em;

    public RoomRepository() {
        em = ConnectionService.getSessionFactory().createEntityManager();
    }

    public List<RoomEntity> findAll() {
        String hqlQuery = "from RoomEntity ";
        return em.createQuery(hqlQuery, RoomEntity.class).getResultList();
    }

    public RoomEntity findOne(Long id) {
        return em.find(RoomEntity.class, id);
    }

    public RoomEntity save(RoomEntity room) {
        return persist(room, em::persist);
    }

    public RoomEntity update(RoomEntity room) {
        return persist(room, em::merge);
    }

    public void delete(RoomEntity room) {
        persist(room, em::remove);
    }

    private RoomEntity persist(RoomEntity room, Consumer<RoomEntity> consumer) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            consumer.accept(room);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }

        return room;
    }
}