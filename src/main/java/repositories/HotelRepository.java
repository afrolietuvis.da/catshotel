package repositories;


import entities.HotelEntity;
import services.ConnectionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.function.Consumer;

public class HotelRepository {
    private final EntityManager em;

    public HotelRepository() {
        em = ConnectionService.getSessionFactory().createEntityManager();
    }

    List<HotelEntity> findAll() {
        String hqlQuery = "from HotelEntity p";
        return em.createQuery(hqlQuery, HotelEntity.class).getResultList();
    }

    public HotelEntity findOne(Long id) {
        return em.find(HotelEntity.class, id);
    }

    public HotelEntity save(HotelEntity hotel) {
        return persist(hotel, em::persist);
    }

    public HotelEntity update(HotelEntity hotel) {
        return persist(hotel, em::merge);
    }

    public void delete(HotelEntity hotel) {
        persist(hotel, em::remove);
    }

    private HotelEntity persist(HotelEntity hotel, Consumer<HotelEntity> consumer) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            consumer.accept(hotel);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }

        return hotel;
    }
}
