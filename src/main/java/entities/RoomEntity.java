package entities;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "room")
public class RoomEntity {

    @Id
    @Column(name = "id",insertable = false,updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "room_type")
    private String roomType;

    @Column(name = "number")
    private int roomNumber;

    @Column(name = "floor",insertable = false,updatable = false)
    private int floorNumber;

    @ManyToOne
    @JoinColumn(name = "hotel_id",nullable = false,insertable = false,updatable = false)
    private HotelEntity hotel;

    @OneToMany
    @JoinColumn(name ="room_id")
    private List<ReservationEntity> reservations = new ArrayList<>();

    @Column(name ="price",nullable = false)
    private int price;


    @Column(name = "description",nullable = false)
    private String description;

    @Column(name ="maximum_guests",nullable = false)
    private int maximumGuests;

    public int getMaximumGuests() {
        return maximumGuests;
    }

    public void setMaximumGuests(int maximumGuests) {
        this.maximumGuests = maximumGuests;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public HotelEntity getHotel() {
        return hotel;
    }

    public void setHotel(HotelEntity hotel) {
        this.hotel = hotel;
    }

    public List<ReservationEntity> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationEntity> reservations) {
        this.reservations = reservations;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public RoomEntity() {
    }

    @Override
    public String toString() {
        return "Room type: " + getRoomType() + ", Floor number: " + getFloorNumber()
                + ", Room number: " + getRoomNumber() + "\nDescription: " +description;
    }

}
