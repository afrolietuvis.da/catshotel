package entities;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "reservation")
public class ReservationEntity {

    @Id
    @Column(name ="id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="date_from")
    private LocalDate startDate;

    @Column(name="date_to")
    private LocalDate endDate;

    @Column(name="is_paid")
    private boolean isPaid;


    @ManyToOne
    @JoinColumn(name ="room_id",nullable = false)
    private RoomEntity room;

    @ManyToOne
    @JoinColumn(name="client_id",nullable = false)
    private ClientEntity client;

    public ReservationEntity(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public RoomEntity getRoom() {
        return room;
    }

    public void setRoom(RoomEntity room) {
        this.room = room;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return
                "Reservation ID: " + id +
                " Checkin date:" + startDate +
                " Checkout date: " + endDate +
                " Room: " + room;
    }
}
