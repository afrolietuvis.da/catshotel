package BigUpdate;

import entities.ClientEntity;
import entities.ReservationEntity;
import entities.RoomEntity;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;
import repositories.ReservationRepository;

import java.time.LocalDate;

public class NewReservationDialogController {

    @FXML
    private Label roomDescriptionLabel;
    @FXML
    private DatePicker checkInPicker;
    @FXML
    private DatePicker checkOutPicker;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;
    @FXML
    private ImageView reservationImageView;

    private Stage primaryStage;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private RoomEntity room;
    private BooleanProperty checkinValid = new SimpleBooleanProperty(false);
    private BooleanProperty checkoutValid = new SimpleBooleanProperty(false);
    private ClientEntity client;

    public NewReservationDialogController() {

    }

    @FXML
    public void initialize() {
        final Callback<DatePicker, DateCell> checkinFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        for (ReservationEntity reservation : room.getReservations()) {
                            if ((item.isAfter(reservation.getStartDate()) || item.equals(reservation.getStartDate()))
                                    && (item.isBefore(reservation.getEndDate()) || item.equals(reservation.getEndDate()))) {
                                setStyle("-fx-background-color: #ff746d;");
                                setDisable(true);
                            }
                        }
                        if (item.isBefore(LocalDate.now())) {
                            setStyle("-fx-background-color: #ff746d;");
                            setDisable(true);
                        }

                    }
                };

            }
        };
        checkInPicker.setDayCellFactory(checkinFactory);

        final Callback<DatePicker, DateCell> checkoutFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(checkInDate)) {
                            setStyle("-fx-background-color: #ff746d;");
                            setDisable(true);
                        }
                        for (ReservationEntity reservation : room.getReservations()) {
                            if (checkInDate.isBefore(reservation.getStartDate())) {
                                if (item.isAfter(reservation.getStartDate()) || item.isEqual(reservation.getStartDate()) || item.isEqual(checkInDate))
                                {
                                    setStyle("-fx-background-color: #ff746d;");
                                    setDisable(true);
                                }
                            }
                        }


                    }
                };

            }
        };
        checkOutPicker.setDayCellFactory(checkoutFactory);

        okButton.disableProperty().bind((checkinValid.and(checkoutValid)).not());
        checkOutPicker.disableProperty().bind(checkinValid.not());

    }

    void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    void setRoom(RoomEntity room, ClientEntity client) {
        roomDescriptionLabel.setText(room.getDescription());
        reservationImageView.setImage(new Image(getClass().getResource("/images/" + room.getRoomType() + ".jpg").toString()));
        this.client = client;
        this.room = room;
    }


    @FXML
    private void handleOk() {
        createNewReservation();
        primaryStage.close();
    }


    @FXML
    private void handleCancel() {
        primaryStage.close();
    }

    @FXML
    void handleCheckinSelection() {
        this.checkInDate = checkInPicker.getValue();
        if (checkInDate != null)
            checkinValid.set(true);
        else checkinValid.set(false);
    }

    @FXML
    void handleCheckoutSelection() {
        this.checkOutDate = checkOutPicker.getValue();
        if (checkOutDate != null)
            checkoutValid.set(true);
        else checkoutValid.set(false);
    }

    private void createNewReservation() {
        ReservationRepository reservationRepository = new ReservationRepository();
        ReservationEntity reservation = new ReservationEntity();
        reservation.setStartDate(this.checkInDate);
        reservation.setEndDate(this.checkOutDate);
        reservation.setRoom(this.room);
        reservation.setPaid(false);
        reservation.setClient(this.client);
        reservationRepository.save(reservation);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(primaryStage);
        alert.setTitle("Success");
        alert.setHeaderText("Reservation successful");
        alert.setContentText("Use the confirmation code we've sent you to " + client.getEmail() + " when checking in.\n\n            Enjoy your stay!");
        alert.showAndWait();

    }

}
