package BigUpdate;

import entities.ClientEntity;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import repositories.ClientRepository;


public class LoginController {

    private Stage primaryStage;
    private ClientRepository clientRepository = new ClientRepository();
    private ClientEntity client;
    private MainApp mainApp;
    @FXML
    Button loginButton;
    @FXML
    Button registerButton;
    @FXML
    TextField idField;
    @FXML
    PasswordField passwordField;

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    @FXML
    private void initialize() {
        passwordField.disableProperty().bind(idField.textProperty().isEmpty());
        loginButton.disableProperty().bind(idField.textProperty().isEmpty().or(passwordField.textProperty().isEmpty()));
    }

    @FXML
    private void handleRegistrationButton() {
        showRegistrationDialog();
    }

    public LoginController() {
    }

        @FXML
        public void validateInput () {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            long id;
            String pw;
            try {
                Long.parseLong(idField.getText());
            } catch (Exception e) {

                alert.initOwner(primaryStage);
                alert.setTitle("Bad input");
                alert.setHeaderText("Error" + e.getMessage());
                alert.setContentText("Id can only contain numbers!");
                alert.showAndWait();
                return;
            }
            id = Long.parseLong(idField.getText());
            pw = passwordField.getText();
            ClientEntity tempClient = clientRepository.findOne(id);
            if (tempClient == null) {
                alert.setTitle("");
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setHeaderText("No client found with this ID");
                alert.setContentText("Please enter valid ID or register");
                alert.showAndWait();
            } else if (tempClient.getPassword().equals(pw)) {
                this.client = tempClient;
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setTitle("Success!");
                alert.setHeaderText("You logged in as: " + tempClient.getName());
                alert.setContentText("Welcome!");
                alert.showAndWait();
                primaryStage.close();
            } else {
                alert.setTitle("");
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setHeaderText("Incorrect password for ID: " + tempClient.getId());
                alert.setContentText("Please enter valid password or click \"remember password\"");
                alert.showAndWait();
            }

        }

      private void showRegistrationDialog() {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("/view/RegistrationDialog.fxml"));
                AnchorPane page = (AnchorPane) loader.load();
                Stage registrationStage = new Stage();
                registrationStage.setTitle("Registration");
                registrationStage.initModality(Modality.WINDOW_MODAL);
                registrationStage.setResizable(false);
                registrationStage.getIcons().add(new Image("/images/hotel32.png"));
                registrationStage.initOwner(primaryStage);


                Scene scene = new Scene(page);
                registrationStage.setScene(scene);

                RegistrationDialogController controller = loader.getController();
                controller.setPrimary(registrationStage);
                controller.setMainApp(this.mainApp);
                registrationStage.showAndWait();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    void setLoginStage(Stage loginStage) {
        this.primaryStage = loginStage;
    }
}
