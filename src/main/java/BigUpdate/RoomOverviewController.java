package BigUpdate;

import entities.ClientEntity;
import entities.RoomEntity;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import repositories.RoomRepository;

import java.io.IOException;

public class RoomOverviewController {
    @FXML
    private TableView<RoomEntity> roomTable;
    @FXML
    private TableColumn<RoomEntity, String> descriptionColumn;

    @FXML
    private Label roomTypeLabel;
    @FXML
    private Label floorNumberLabel;
    @FXML
    private Label roomNumberLabel;
    @FXML
    private Label maximumGuestsLabel;
    @FXML
    private Label pricePerNightLabel;
    @FXML
    private ImageView roomImageView;
    @FXML
    Button newReservationButton;
    @FXML
    Label loggedInAsLabel;
    @FXML
    Button loginButton;

    private MainApp mainApp;
    private ObjectProperty<ClientEntity> clientLoggedOn = new SimpleObjectProperty<>();
    private Stage primaryStage;


    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public RoomOverviewController() {
    }


    public void setClient(ClientEntity clientLoggedOn) {
        this.clientLoggedOn.set(clientLoggedOn);
    }

    @FXML
    private void initialize() {

        descriptionColumn.setCellValueFactory(
                new PropertyValueFactory<>("description")
        );
        showRoomDetails(null);
        roomTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showRoomDetails(newValue));

        roomTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showImage(newValue));
        clientLoggedOn.addListener((
                (observable, oldValue, newValue) -> {
                    if(clientLoggedOn.get() == null){
                        loggedInAsLabel.setText("You are not logged in.");
                        loginButton.setText("Log In");
                    } else {
                        loggedInAsLabel.setText("Logged in as: " +newValue.getName());
                        loginButton.setText("Log out");
                    }
                }
                ));



    }
    @FXML
   public void handleLoginButton(){
        if(this.loginButton.getText().equals("Log out")){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(this.mainApp.getPrimaryStage());
            alert.setTitle("Bye!");
            alert.setHeaderText("You logged out");
            alert.setContentText("Hope you come back soon!");
            alert.showAndWait();
            this.clientLoggedOn.set(null);
        } else{
        showLoginDialog();
        }

    }


    void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        roomTable.setItems(mainApp.getRoomData());
    }

    private void showRoomDetails(RoomEntity room) {
        if (room != null) {
            roomNumberLabel.setText(String.valueOf(room.getRoomNumber()));
            roomTypeLabel.setText(String.valueOf(room.getRoomType()));
            floorNumberLabel.setText(String.valueOf(room.getFloorNumber()));
            maximumGuestsLabel.setText(String.valueOf(room.getMaximumGuests()));
            pricePerNightLabel.setText(room.getPrice() + "$");
        } else {
            roomNumberLabel.setText("");
            roomTypeLabel.setText("");
            floorNumberLabel.setText("");
            maximumGuestsLabel.setText("");
            pricePerNightLabel.setText("");
        }
    }

    private void showImage(RoomEntity room) {
        Image image = new Image(getClass().getResource("/images/" + room.getRoomType() + ".jpg").toString());
        roomImageView.setImage(image);
    }

    @FXML
    public void makeNewReservation() {
        RoomEntity room = roomTable.getSelectionModel().getSelectedItem();
        int selectedIndex = roomTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            showNewReservationDialog(room);
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Room Selected");
            alert.setContentText("Please select a room from the list.");
            alert.showAndWait();
        }


    }

    @FXML
    private void showNewReservationDialog(RoomEntity room){
        try{
            RoomRepository roomRepository = new RoomRepository();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/NewReservationDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("New Reservation");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            dialogStage.getIcons().add(new Image("/images/hotel32.png"));
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            dialogStage.setResizable(false);

            NewReservationDialogController controller = loader.getController();
            controller.setPrimaryStage(dialogStage);
            if(this.clientLoggedOn.get() != null)
            controller.setRoom(roomRepository.findOne(room.getId()),this.clientLoggedOn.get());
            else{

                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(primaryStage);
                alert.setTitle("Error");
                alert.setHeaderText("You are not logged in");
                alert.setContentText("Please login or register before making a reservation");
                alert.showAndWait();
                return;
            }

            dialogStage.showAndWait();

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void showLoginDialog(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/LoginDialog.fxml"));

            AnchorPane page = (AnchorPane) loader.load();
            Stage loginStage = new Stage();
            loginStage.setTitle("Logging in");
            loginStage.initModality(Modality.WINDOW_MODAL);
            loginStage.setResizable(false);
            loginStage.initOwner(primaryStage);
            loginStage.getIcons().add(new Image("/images/hotel32.png"));
            Scene scene = new Scene(page);
            loginStage.setScene(scene);

            LoginController controller = loader.getController();
            controller.setLoginStage(loginStage);
            controller.setMainApp(this.mainApp);
            loginStage.showAndWait();
            this.setClient(controller.getClient());

        }catch (IOException e){
            e.printStackTrace();
        }
    }


}
