package BigUpdate;

import entities.ClientEntity;
import entities.RoomEntity;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import repositories.RoomRepository;

import java.io.IOException;

public class MainApp extends Application {



    private RoomRepository roomRepository;
    private ObservableList<RoomEntity> rooms;
    private Stage primaryStage;
    private BorderPane rootLayout;

    public MainApp(){
        roomRepository = new RoomRepository();
        rooms = FXCollections.observableArrayList(roomRepository.findAll());
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Hotel App");
        this.primaryStage.getIcons().add(new Image("/images/hotel32.png"));
        initRootLayout();
        showRoomOverview();
    }

    private void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/RootLayout.fxml"));

            rootLayout = (BorderPane) loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            RootController controller = new RootController();
            controller.setMainApp(this);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void showRoomOverview() {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/RoomOverview.fxml"));
            AnchorPane hotelOverview = (AnchorPane) loader.load();
            rootLayout.setCenter(hotelOverview);
            RoomOverviewController controller = loader.getController();
            controller.setPrimaryStage(primaryStage);
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    ObservableList<RoomEntity> getRoomData(){
        return this.rooms;
    }

}
