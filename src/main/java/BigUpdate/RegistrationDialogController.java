package BigUpdate;

import entities.ClientEntity;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import repositories.ClientRepository;

import java.util.List;

public class RegistrationDialogController {

    private Stage primaryStage;
    private ClientRepository clientRepository = new ClientRepository();
    private ClientEntity newClient = new ClientEntity();
    private MainApp mainApp;


    @FXML TextField nameField;
    @FXML TextField emailField;
    @FXML TextField phoneField;
    @FXML PasswordField passwordField;
    @FXML PasswordField passwordField2;

    @FXML Label validateNameLabel;
    @FXML Label validatePasswordLabel;
    @FXML Label validatePassword2Label;
    @FXML Label validateEmailLabel;
    @FXML Label validatePhoneLabel;

    @FXML Button cancelButton;
    @FXML Button submitButton;





@FXML
private void initialize(){
    submitButton.disableProperty().bind(validateSubmit());
}

    void setPrimary(Stage stage) {
        this.primaryStage = stage;
    }


    void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }



    @FXML void validatePassword(){
        if (passwordField.getText().length()<7){
            validatePasswordLabel.setTextFill(Color.RED);
            validatePasswordLabel.setText("Password is too short");
        } else {
            validatePasswordLabel.setTextFill(Color.GREEN);
            validatePasswordLabel.setText("OK");
        }
    }
    @FXML void validatePassword2(){
        boolean match = passwordField.getText().equals(passwordField2.getText());
        if (match){
            validatePassword2Label.setTextFill(Color.GREEN);
            validatePassword2Label.setText("OK");
        } else {
            validatePassword2Label.setTextFill(Color.RED);
            validatePassword2Label.setText("Passwords don't match");
        }
    }

    @FXML void validateName(){
        if (nameField.getText().length()<1){
            validateNameLabel.setTextFill(Color.RED);
            validateNameLabel.setText("Please enter your name");
        } else {
            validateNameLabel.setTextFill(Color.GREEN);
            validateNameLabel.setText("OK");
        }
    }
    @FXML void validatePhone(){
        if (phoneField.getText().length()<9){
            validatePhoneLabel.setTextFill(Color.RED);
            validatePhoneLabel.setText("Invalid phone number");
        } else {
            validatePhoneLabel.setTextFill(Color.GREEN);
            validatePhoneLabel.setText("OK");
        }
    }
    @FXML void validateEmail(){
        if (!emailField.getText().contains("@") || !emailField.getText().contains(".com")){
            validateEmailLabel.setTextFill(Color.RED);
            validateEmailLabel.setText("Invalid email address");
        } else {
            validateEmailLabel.setTextFill(Color.GREEN);
            validateEmailLabel.setText("OK");
        }
    }
@FXML
    private void checkIfUniqueEmailAndPhoneNumber(){
    String phone = phoneField.getText();
    String email = emailField.getText();
        List<ClientEntity> clients = clientRepository.findAll();
        for(ClientEntity clientEntity:clients){
            if (clientEntity.getPhone().equals(phone)){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(primaryStage);
                alert.setTitle("Phone validation error");
                alert.setHeaderText("User with this phone number already registered");
                alert.setContentText("Please enter new phone number");
                alert.showAndWait();
                return;
            } if (clientEntity.getEmail().equals(email)){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(primaryStage);
                alert.setTitle("Email validation error");
                alert.setHeaderText("User with this email already registered");
                alert.setContentText("Please enter new email address");
                alert.showAndWait();
                return;
            }
        }
    createNewClient();
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.initOwner(primaryStage);
    alert.setTitle("Success");
    alert.setHeaderText("Registration successful");
    alert.setContentText("your ID is " + newClient.getId() +". Use it to login with your password");
    alert.showAndWait();
    primaryStage.close();

    }
    private ObservableValue<? extends Boolean> validateSubmit(){
        return (validatePhoneLabel.textProperty().isNotEqualTo("OK")
                .or(validateEmailLabel.textProperty().isNotEqualTo("OK"))
                .or(validateNameLabel.textProperty().isNotEqualTo("OK"))
                .or(validatePasswordLabel.textProperty().isNotEqualTo("OK"))
                .or(validatePassword2Label.textProperty().isNotEqualTo("OK")));
    }
    private ClientEntity getClientIdByPhoneNumber(String phone){
        List<ClientEntity> clients = clientRepository.findAll();
        for(ClientEntity clientEntity:clients){
            if (clientEntity.getPhone().equals(phone)){
                return clientEntity;
            }
        }
        return null;
    }

    private void createNewClient(){
        newClient.setName(nameField.getText());
        newClient.setPassword(passwordField.getText());
        newClient.setEmail(emailField.getText());
        newClient.setPhone(phoneField.getText());
        clientRepository.save(newClient);
        newClient = getClientIdByPhoneNumber(newClient.getPhone());
    }


    public RegistrationDialogController(){

    }
}
