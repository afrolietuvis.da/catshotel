package services;

import java.util.Properties;


import entities.ClientEntity;
import entities.HotelEntity;
import entities.ReservationEntity;
import entities.RoomEntity;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

public final class ConnectionService {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();

            Properties settings = new Properties();
            settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
            settings.put(Environment.URL, "jdbc:mysql://localhost:3306/catshotel?useTimezone=true&serverTimezone=UTC");
            settings.put(Environment.USER, "root");
            settings.put(Environment.PASS, "StrongPassword123");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
            settings.put(Environment.SHOW_SQL, "true");
            settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
            settings.put(Environment.HBM2DDL_AUTO, "validate");
            settings.put(Environment.ALLOW_JTA_TRANSACTION_ACCESS, "true");
            configuration.setProperties(settings);


            configuration.addAnnotatedClass(ClientEntity.class);
            configuration.addAnnotatedClass(RoomEntity.class);
            configuration.addAnnotatedClass(HotelEntity.class);
            configuration.addAnnotatedClass(ReservationEntity.class);



            sessionFactory = configuration.buildSessionFactory(
                    new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build()
            );
        }

        return sessionFactory;
    }

    public ConnectionService() {
    }
}

