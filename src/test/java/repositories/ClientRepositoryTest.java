package repositories;

import entities.ClientEntity;
import entities.HotelEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;


public class ClientRepositoryTest {

    private ClientRepository clientRepository;

    @BeforeEach
    void setup() throws SQLException {
        clientRepository = new ClientRepository();
    }


    @Test
    void findAll(){
        List<ClientEntity> result = clientRepository.findAll();
        assertFalse(result.isEmpty());
    }
}
