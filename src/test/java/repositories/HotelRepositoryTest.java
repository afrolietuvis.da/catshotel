package repositories;

import entities.HotelEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class HotelRepositoryTest {

    private HotelRepository hotelRepository;

    @BeforeEach
    void setup() throws SQLException{
        hotelRepository = new HotelRepository();
    }


    @Test
    void findAll(){
        List<HotelEntity> result = hotelRepository.findAll();
        assertFalse(result.isEmpty());
    }

}
