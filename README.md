### Code base demo for Practical Projects 

This project is based on a GitLab [Project Template]
(https://gitlab.com/wsaulius/my3project.git).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps]
(https://docs.gitlab.com/ee/topics/autodevops/).

Team for project is as follows: 

    Deivydas (CodeCats)  - frontend FAVAFX   https://gitlab.com/afrolietuvis.da/
    Egle - SME *Subject Matter expert, JUnit tests (Jupiter/JUnit5)   https://gitlab.com/eglaite
    Sarunas - DAO modelling, Data layer   https://gitlab.com/sarunas_svedas
    Ramunas - (Design Patterns, Class desing, good code, core)   https://gitlab.com/ramunas.songaila
     
    
    
- Plan for the day 1 

    my3projects repo 
    
    README.md 
    Send me all your git-repo pointers  
    
    Configurure Hibernate in project: cfg file in XML 
    
	1. Create a Hibernate XML configuration file that points to your database and your mapped
	classes.  (Listing 2-4.) 
	<property name="hbm2ddl.auto">create</property> 
    
    - DML + DDL ==> Hibernate 
    
 	1.Identify the POJOs that have a database representation. (restrict to one-to-many) 
	2. Identify which properties of those POJOs need to be persisted. (define fields in tables) 
	3. Annotate each of the POJOs to map your Java object’s properties to columns in a database
	table (add annotations in @Entity POJOs) 
	4. Create the database schema using the schema export tool, use an existing database, or
	create your own database schema. (Produce a physical DB from POJOs in Java) 
	
	5. Add the Hibernate Java libraries to your application’s classpath (maven).     
	6. Dump current DB state to project .sql (to be re-used) 
	



    